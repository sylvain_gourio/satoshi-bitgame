import Vue from 'vue';

const accountService = {
  account: null,
  getAccount() {
    return Vue.http.get('/api/accounts/mine')
    .then(response => response.json())
    .then((account) => {
      this.account = account;
      return account;
    });
  },
  getAccounts() {
    return Vue.http.get('/api/accounts')
    .then(response => response.json());
  },
  getMember() {
    return Vue.http.get('/api/member/me')
    .then(response => response.json());
  },
  postOrder(currency, orderType, quantity) {
    return Vue.http.post('/api/orders', {
      currency,
      orderType,
      quantity,
    }).then(response => response.json())
    .then((response) => {
      this.account = response[0];
      return response[0];
    });
  },
};

export default accountService;
