import Vue from 'vue';
import VueResource from 'vue-resource';

Vue.use(VueResource);

const userService = {
  user: null,
  refreshing: false,
  refreshUser() {
    this.refreshing = true;
    return new Promise((resolve, reject) => {
      Vue.http.get('/oauth/user')
        .then(response => response.json())
        .then((user) => {
          this.refreshing = false;
          if (user && user.displayName) {
            this.user = user;
            resolve(user);
          }
        })
        .catch((err) => {
          this.refreshing = false;
          reject(err);
        });
    });
  },
};

userService.refreshUser();

export default userService;
