// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import VueCharts from 'vue-charts';
import App from './App';
import Home from './components/Home';
import Game from './components/Game';
import Gamers from './components/Gamers';
import Stats from './components/Stats';
import Orders from './components/Orders';
import UseApi from './components/Use-api';
import Simulator from './components/Simulator';
import TradingPlateform from './components/Trading-plateform';


Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VueCharts);

Vue.filter('satoshi', (value) => {
  if (!value) return '0';
  return (value * 100).toFixed(3);
});

const routes = [
  { path: '/', component: Home, name: 'home' },
  { path: '/game', component: Game, name: 'game' },
  { path: '/gamers', component: Gamers, name: 'gamers' },
  { path: '/stats', component: Stats, name: 'stats' },
  { path: '/trading-plateform', component: TradingPlateform, name: 'trading-plateform' },
  { path: '/use-api', component: UseApi, name: 'use-api' },
  { path: '/orders', component: Orders, name: 'orders' },
  { path: '/simulator', component: Simulator, name: 'simulator' },
];

const router = new VueRouter({
  routes,
});

Vue.http.interceptors.push((request, next) => {
  next((response) => {
    if (response.status === 401) {
      router.push('/');
    }
  });
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router,
});
