import Vue from 'vue';

const marketService = {
  markets: null,
  getMarkets() {
    return Vue.http.get('/api/getmarketsummaries')
    .then(response => response.json())
    .then((markets) => {
      this.markets = markets;
      return markets;
    });
  },
};

export default marketService;
